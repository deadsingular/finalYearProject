from pyclausie import ClausIE
import nltk

def processContent(item):
  try:
      tokenized = nltk.word_tokenize(item)
      tagged = nltk.pos_tag(tokenized)
      #chunkGram = r"""Chunk: {<RB.?>*<VB.?>*<NNP>}"""
      chunkGram = r"""Chunk: {<VB>|<VBG>|<VBP>|<VBZ>}"""
      chunkParser = nltk.RegexpParser(chunkGram)
      chunked = chunkParser.parse(tagged)
      x = str(chunked)
      if 'Chunk' in x:
        return 1
      else:
        return 0
  except Exception as e:
    print str(e)

cl = ClausIE.get_instance()
#sents = ['I learned that the 2012 Sasquatch music festival is scheduled for May 25th until May 28.']
#sents = map(list, raw_input('Enter a file name: '))
#sent = raw_input('Enter a file name: ')
sents = []
sents.append(raw_input('Enter a file name: '))
print type(sents) 
triples = cl.extract_triples(sents)
obj = []

for triple in triples:
  #print triple
  #obj.append(triple.predicate)
  #processContent(obj)
  if processContent(triple.predicate):
    print triple
  else:
    print "Not it"
  #tk = nltk.word_tokenize(obj)
  #tagged = nltk.pos_tag(tk)
  #print tagged



