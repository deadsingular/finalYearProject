{'action': 'operating', 'phrase': [('CPU', 'NN'), ('processes', u'NNS'), ('operating', u'VBG'), ('system', u'NN')], 'object': 'system', 'subject': 'cpu'}
{'action': 'interact', 'phrase': [('CPU', 'NN'), ('among', u'IN'), ('processes', u'NNS'), ('frequently', u'RB'), ('users', u'NNS'), ('interact', u'VB'), ('program', u'NN')], 'object': 'program', 'subject': 'cpu'}
{'action': 'selects', 'phrase': [('CPU', 'NN'), ('scheduler', 'NN'), ('selects', u'VBZ'), ('among', u'IN'), ('processes', u'NNS')], 'object': 'processes', 'subject': 'cpu'}
{'action': 'identified', 'phrase': [('UNIX', 'NN'), ('process', u'NN'), ('identified', u'VBD'), ('process', u'NN')], 'object': 'process', 'subject': 'unix'}
{'action': 'occurs', 'phrase': [('Termination', 'NN'), ('occurs', u'VBZ'), ('additional', u'JJ'), ('circumstances', u'NNS')], 'object': 'circumstances', 'subject': 'termination'}
{'action': 'provides', 'phrase': [('IPC', 'NN'), ('provides', u'VBZ'), ('mechanism', u'NN')], 'object': 'mechanism', 'subject': 'ipc'}
{'action': 'provides', 'phrase': [('IPC', 'NN'), ('facility', 'NN'), ('provides', u'VBZ'), ('least', u'AP'), ('two', u'CD'), ('operations', u'NNS')], 'object': 'operations', 'subject': 'ipc'}
{'action': 'receive', 'phrase': [('send', u'VB'), ('messages', u'NNS'), ('receive', u'VBP'), ('messages', u'NNS')], 'object': 'messages', 'subject': 'send'}
{'action': 'receive', 'phrase': [('send', u'VB'), ('receive', u'VB'), ('operations', u'NNS')], 'object': 'operations', 'subject': 'send'}
{'action': 'blocking', 'phrase': [('Message', u'NN-TL'), ('passing', u'VBG'), ('may', u'MD'), ('either', u'CC'), ('blocking', u'VBG'), ('nonblocking', 'NN')], 'object': 'nonblocking', 'subject': 'message'}
{'action': 'receiving', 'phrase': [('message', 'NN'), ('received', u'VBD'), ('receiving', u'VBG'), ('process', u'NN')], 'object': 'process', 'subject': 'message'}
{'action': 'calls', 'phrase': [('Mach', 'NN'), ('including', u'IN'), ('system', u'NN'), ('calls', u'VBZ'), ('intertask', 'NN')], 'object': 'intertask', 'subject': 'mach'}
{'action': 'uses', 'phrase': [('kernel', 'NN'), ('uses', u'VBZ'), ('Kernel', 'NN')], 'object': 'Kernel', 'subject': 'kernel'}
{'action': 'sends', 'phrase': [('kernel', 'NN'), ('sends', u'VBZ'), ('notification', 'NN')], 'object': 'notification', 'subject': 'kernel'}
{'action': 'executed', 'phrase': [('RPCs', 'NN'), ('executed', u'VBD'), ('via', u'IN'), ('msg', 'NN')], 'object': 'msg', 'subject': 'rpcs'}
{'action': 'distributed', 'phrase': [('Mach', 'NN'), ('system', u'NN'), ('especially', u'RB'), ('designed', u'VBD'), ('distributed', u'VBN'), ('systems', u'NNS')], 'object': 'systems', 'subject': 'mach'}
{'action': 'avoid', 'phrase': [('Mach', 'NN'), ('message', 'NN'), ('system', u'NN'), ('attempts', u'VBZ'), ('avoid', u'VB'), ('double-copy', 'NN')], 'object': 'double-copy', 'subject': 'mach'}
{'action': 'provides', 'phrase': [('Windows', 'NN'), ('provides', u'VBZ'), ('support', u'NN')], 'object': 'support', 'subject': 'windows'}
{'action': 'called', 'phrase': [('Windows', 'NN'), ('called', u'VBN'), ('local', u'JJ'), ('procedure-', 'NN')], 'object': 'procedure-', 'subject': 'windows'}
{'action': 'communicate', 'phrase': [('PrintWriter', 'NN'), ('object', u'NN'), ('use', u'NN'), ('communicate', u'VB'), ('client', 'NN')], 'object': 'client', 'subject': 'printwriter'}
{'action': 'allows', 'phrase': [('PrintWriter', 'NN'), ('object', u'NN'), ('allows', u'VBZ'), ('server', 'NN')], 'object': 'server', 'subject': 'printwriter'}
{'action': 'discussed', 'phrase': [('RPC', 'NN'), ('paradigm', 'NN'), ('discussed', u'VBN'), ('briefly', u'NN')], 'object': 'briefly', 'subject': 'rpc'}
{'action': 'exchanged', 'phrase': [('IPC', 'NN'), ('facility', 'NN'), ('messages', u'NNS'), ('exchanged', u'VBN'), ('RPC', 'NN')], 'object': 'RPC', 'subject': 'ipc'}
{'action': 'received', 'phrase': [('RPC', 'NN'), ('message', 'NN'), ('port', u'NN'), ('server', 'NN'), ('data', u'NNS'), ('would', u'MD'), ('received', u'VBN'), ('reply', u'NN')], 'object': 'reply', 'subject': 'rpc'}
{'action': 'allowing', 'phrase': [('RPC', 'NN'), ('system', u'NN'), ('hides', u'NNS'), ('necessary', u'JJ'), ('details', u'NNS'), ('allowing', u'VBG'), ('communication', 'NN')], 'object': 'communication', 'subject': 'rpc'}
{'action': 'providing', 'phrase': [('RPC', 'NN'), ('system', u'NN'), ('providing', u'VBG'), ('stub', 'NN')], 'object': 'stub', 'subject': 'rpc'}
{'action': 'sent', 'phrase': [('XCR', 'NN'), ('sent', u'VBD'), ('server', 'NN')], 'object': 'server', 'subject': 'xcr'}
{'action': 'converted', 'phrase': [('XCR', 'NN'), ('data', u'NNS'), ('unmarshalled', 'NN'), ('converted', u'VBN'), ('machine-dependent', 'NN')], 'object': 'machine-dependent', 'subject': 'xcr'}
{'action': 'requires', 'phrase': [('RPC', 'NN'), ('scheme', 'NN'), ('requires', u'VBZ'), ('similar', u'JJ'), ('binding', u'NN')], 'object': 'binding', 'subject': 'rpc'}
{'action': 'distributed', 'phrase': [('RPC', 'NN'), ('scheme', 'NN'), ('useful', u'JJ'), ('implementing', u'VBG'), ('distributed', u'VBN'), ('file', u'NN')], 'object': 'file', 'subject': 'rpc'}
{'action': 'connected', 'phrase': [('JVM', 'NN'), ('computer', 'NN'), ('remote', u'JJ'), ('host', 'NN'), ('connected', u'VBN'), ('network', u'NN')], 'object': 'network', 'subject': 'jvm'}
{'action': 'differ', 'phrase': [('RMI', 'NN'), ('RPCs', 'NN'), ('differ', u'VB'), ('two', u'CD'), ('fundamental', u'JJ'), ('ways', u'NNS')], 'object': 'ways', 'subject': 'rmi'}
{'action': 'supports', 'phrase': [('RMI', 'NN'), ('object-based', 'NN'), ('It', u'PPS'), ('supports', u'VBZ'), ('invocation', 'NN')], 'object': 'invocation', 'subject': 'rmi'}
{'action': 'objects', 'phrase': [('RMI', 'NN'), ('possible', u'JJ'), ('pass', u'NN'), ('objects', u'VBZ'), ('parameters', u'NNS')], 'object': 'parameters', 'subject': 'rmi'}
{'action': 'resides', 'phrase': [('someMethod', 'NN'), ('resides', u'VBZ'), ('server', 'NN')], 'object': 'server', 'subject': 'somemethod'}
{'action': 'sends', 'phrase': [('someMethod', 'NN'), ('sends', u'VBZ'), ('value', u'NN')], 'object': 'value', 'subject': 'somemethod'}
{'action': 'makes', 'phrase': [('RMI', 'NN'), ('provides', u'VBZ'), ('makes', u'VBZ'), ('stubs', u'NNS')], 'object': 'stubs', 'subject': 'rmi'}
{'action': 'allows', 'phrase': [('RMI', 'NN'), ('allows', u'VBZ'), ('thread', 'NN')], 'object': 'thread', 'subject': 'rmi'}
{'action': 'allows', 'phrase': [('RMI', 'NN'), ('allows', u'VBZ'), ('objects', u'NNS')], 'object': 'objects', 'subject': 'rmi'}
