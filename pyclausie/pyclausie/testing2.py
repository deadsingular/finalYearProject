from pyclausie import ClausIE
import nltk

def processContent(examp):
  try:
    for item in examp:
      tokenized = nltk.word_tokenize(item)
      tagged = nltk.pos_tag(tokenized)
      chunkGram = r"""Predicate: {<VB>|<VBG>|<VBP>|<VBZ>}"""
      chunkParser = nltk.RegexpParser(chunkGram)
      chunked = chunkParser.parse(tagged)
      x = str(chunked)
      print x
  except Exception as e:
    print str(e)

cl = ClausIE.get_instance()
sents = []
sents.append(raw_input('Enter a file name: '))
triples = cl.extract_triples(sents)
obj = []

for triple in triples:
  print triple
  obj.append(triple.predicate)
  processContent(obj)
  obj.pop(0)


