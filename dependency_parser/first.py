#!/usr/bin/python3

from nltk.parse.stanford import StanfordParser
from nltk.parse.stanford import StanfordDependencyParser


parser=StanfordParser(model_path="edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz")
list(parser.raw_parse("the quick brown fox jumps over the lazy dog"))
'''
[Tree('ROOT', [Tree('NP', [Tree('NP', [Tree('DT', ['the']), Tree('JJ', ['quick']), Tree('JJ', ['brown']), Tree('NN', ['fox'])]), Tree('NP', [Tree('NP', [Tree('NNS', ['jumps'])]), Tree('PP', [Tree('IN', ['over']), Tree('NP', [Tree('DT', ['the']), Tree('JJ', ['lazy']), Tree('NN', ['dog'])])])])])])]
'''

dependency_parser = StanfordDependencyParser()
[list(parse.triples()) for parse in dependency_parser.raw_parse("The quick brown fox jumps over the lazy dog.")]
'''
[[(('jumps', 'VBZ'), 'nsubj', ('fox', 'NN')), (('fox', 'NN'), 'det', ('The', 'DT')), (('fox', 'NN'), 'amod', ('quick', 'JJ')), (('fox', 'NN'), 'amod', ('brown', 'JJ')), (('jumps', 'VBZ'), 'nmod', ('dog', 'NN')), (('dog', 'NN'), 'case', ('over', 'IN')), (('dog', 'NN'), 'det', ('the', 'DT')), (('dog', 'NN'), 'amod', ('lazy', 'JJ'))]]
'''

