#!/usr/bin/env python3

import PyPDF2 as pypdf
import re


pdffile = open('os.pd', 'rb')
pdfreader = pypdf.PdfFileReader(pdffile)
n = pdfreader.numPages

writefile = open('test.txt', 'w')
for i in range(0,n):
    page = pdfreader.getPage(i)
    writefile.write(page.extractText())


