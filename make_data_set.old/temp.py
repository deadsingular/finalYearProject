#!/usr/bin/env python3

import PyPDF2 as pypdf
import re
import csv

### Generate text file from PDF
# pdffile = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/os.pdf', 'rb')
# pdfreader = pypdf.PdfFileReader(pdffile)
# n = pdfreader.numPages
# writefile = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/test.txt', 'w')
# for i in range(0,n):
#     page = pdfreader.getPage(i)
#     text = page.extractText()
#     #matchobj = re.match("^Contents.*", text[0])
#     #match = re.search("(^Contents).*(APPENDICES)", contactInfo)
#     #print(match.group(0))
#     writefile.write(text)


# Chapter 1 extraction
file = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/test.txt', 'r')
lines = file.readlines()
contentsMatchEndRegex = re.search(".*(Chapter 1 Introduction)", lines[0])
contentEnd = contentsMatchEndRegex.group(0)
f = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/Chap1.txt', 'w')
val = contentEnd.split("Introduction", 3)
f.write(val[3])
f.close()
file.close()

# Chapter 1  put into CSV
file = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/Chap1.txt', 'r')
lines = file.readlines()
sentences = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', lines[0])
with open('test2.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
    i = 1
    for sent in sentences:
        spamwriter.writerow([i, sent])
        i = i + 1
#f = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/Chap1_sep.txt', 'w')
#for sentence in sentences:
#    f.write(sentence + "\n")




# Chapter 2 extraction
file = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/test.txt', 'r')
lines = file.readlines()
contentsMatchEndRegex = re.search(".*(Chapter 2 Operating-System Structures)", lines[0])
contentEnd = contentsMatchEndRegex.group(0)
f = open('/Users/deadsingular/Desktop/FinalYearProject/make_data_set/Chap2.txt', 'w')
#val = contentEnd.split("Operating-System Structures", 1)
f.write(contentEnd)



#contentsMatchStartRegex = re.search("(Contents).*", contentEnd)
#contents = contentsMatchStartRegex.group(0)
#print(contents)
