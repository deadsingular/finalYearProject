#!/bin/bash
input="file.txt"
while IFS= read -r var
do
  op=`python sentiment.py $var | grep objective | awk {'print $2'}`
  echo "$var     $op"
done < "$input"
