#!/usr/bin/env python3

#####################################################
# USAGE: python3 textExtractor.py Operating_Sys.pdf #
#                                                   #
#   This script takes as input a pdf file, and      #
# extracts the text, splits it into chapters and    #
# fills each chapter file with its sentences.       #
#                                                   #
# Prerequisites for running this script:            #
#   certifi==2017.7.27.1                            #
#   nltk==3.2.4                                     #
#   numpy==1.13.1                                   #
#   PyPDF2==1.26.0                                  #
#   six==1.10.0                                     #
#   stemming==1.0.1                                 #
#   xlwt==1.2.0                                     #
#####################################################

import PyPDF2 as pypdf
import re
import nltk
from sys import argv

if(len(argv) != 2):
    print("USAGE: python3 textExtractor.py file.pdf")
    exit(1)

bookname = argv[1]

pdffile = open(bookname, 'rb')
pdfreader = pypdf.PdfFileReader(pdffile)
n = pdfreader.numPages

writefile = open('pages.txt', 'w')
for i in range(0,n):
    page = pdfreader.getPage(i)
    writefile.write(page.extractText() + "\n")


 
f = open("pages.txt", "r")
lines = f.readlines()
c = open("contents.txt", "w")
for line in lines:
    if re.search("^[xvi]* Contents", line):
        c.write(line + "\n")
    elif re.search("^Contents", line):
        c.write(line + "\n")
'''
f = open("contents.txt", "r")
lines = f.readlines()
for line in lines:
    patt = re.split("(Chapter\s[0-9]*\s[A-Za-z\- \/]*)", line)
    if patt:
        #contentEnd = patt.group(1)
        for item in patt:
            print(item)

    #patt = re.search(".*(Chapter\s[0-9]*\s[A-Za-z\- \/]*).*", line)
    #if patt:
    #    contentEnd = patt.group(1)
    #    print(contentEnd)
    #patt = re.search("[0-9]{1,5}(Chapter\s[0-9]*\s[A-Za-z\- \/]*).*", line)
    #if patt:
    #    contentEnd = patt.group(1)
    #    print(contentEnd + " 3")
    #patt = re.search("[A-Z]{1,20}(Chapter\s[0-9]*\s[A-Za-z\- \/]*).*", line)
    #if patt:
    #    contentEnd = patt.group(1)
    #    print(contentEnd + " 4")
'''

chaplist = open("chapList.txt", "r")
chaps = chaplist.readlines()
for chap in chaps:
    parts = chap.split()
    blah, number, name = parts[0], parts[1], ' '.join(parts[2:])
    #print(name)

    my_regex = r"(^[0-9]* ?" + re.escape(blah) + " " + re.escape(number) + " " + re.escape(name) + ")|(^" + re.escape(number) + "\.)"

    f = open("pages.txt", "r")
    filename = blah + number + ".txt"
    f1 = open(filename, "w")
    lines = f.readlines()
    for line in lines:
        if re.search(my_regex, line):
           clean_line = re.sub(r'[^\x00-\x7f]+', ' ', line)
           sentences = nltk.sent_tokenize(clean_line)
           for sentence in sentences:
               f1.write(sentence + "\n")


#doc = open('Chapter12.txt', 'r')
#document = doc.readlines()
#for pages in document:
#    sentences = nltk.sent_tokenize(pages)
#    print sentences


